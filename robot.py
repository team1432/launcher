import wpilib
from networktables import NetworkTable
from oi import OI
from robot_map import RobotMap


class MyRobot(wpilib.IterativeRobot):

	def robotInit(self):
		"""
		This function is called upon program startup and
		should be used for any initialization code.
		"""

		# self.camera = wpilib.USBCamera(name='cam1')
		# self.server = wpilib.CameraServer.getInstance()
		# self.server.setQuality(50)
		# self.server.startAutomaticCapture(self.camera)

		# Create operator interface
		self.oi = OI(self)

		self.lowerMotorSpark = wpilib.Spark(RobotMap.lowerMotor)
		self.upperMotorSpark = wpilib.Spark(RobotMap.upperMotor)

		self.sd = NetworkTable.getTable("SmartDashboard")
		self.sd.putNumber('upperMotorSpeed', 0)
		self.sd.putNumber('lowerMotorSpeed', 0)

	def autonomousInit(self):
		pass

	def autonomousPeriodic(self):
		"""This function is called periodically during autonomous."""
		pass

	def teleopPeriodic(self):
		"""This function is called periodically during operator control."""
		oldLower = self.sd.getNumber('lowerMotorSpeed')
		oldUpper = self.sd.getNumber('upperMotorSpeed')
		
		lowerStick = (self.oi.stick0.getY()/2)
		upperStick = (self.oi.stick1.getY()/2)
		
		if -0.05 < lowerStick < 0.05:		
			lowerStick = 0

		if -0.05 < upperStick < 0.05:
			upperStick = 0

		newLower = oldLower - lowerStick
		newUpper = oldUpper - upperStick
		
		print(newLower)
		print(newUpper)
		
		if newLower > 100:
			newLower = 100
		if newLower < 0:
			newLower = 0

		if newUpper > 100:
			newUpper = 100
		if newUpper < 0:
			newUpper = 0

		if self.oi.stick0.getButton(0):
			newLower = 0
			
		if self.oi.stick1.getButton(0):
			newUpper = 0
		
		self.sd.putNumber('lowerMotorSpeed', newLower)
		self.sd.putNumber('upperMotorSpeed', newUpper)

		self.lowerMotorSpark.set(newLower)
		self.upperMotorSpark.set(newUpper)

	def testPeriodic(self):
		"""This function is called periodically during test mode."""
		pass

if __name__ == "__main__":
	wpilib.run(MyRobot)
